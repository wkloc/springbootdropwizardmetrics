package com.pgs.config;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;

@Configuration
@EnableMetrics
public class MetricsConfig extends MetricsConfigurerAdapter {

	@Value("${csv.path}")
    private String csvPath;
	@Value("${reporter.interval}") 
	private Integer reporterInterval;
	
    @Override
    public void configureReporters(MetricRegistry metricRegistry) {
        // registerReporter allows the MetricsConfigurerAdapter to
        // shut down the reporter when the Spring context is closed
        
    	registerReporter(ConsoleReporter
            .forRegistry(metricRegistry)
            .build())
            .start(reporterInterval, TimeUnit.SECONDS);
    	
    	registerReporter(JmxReporter
                .forRegistry(metricRegistry)
                .build())
                .start();
    	
    	registerReporter(CsvReporter
    			.forRegistry(metricRegistry)
    			.formatFor(Locale.US)
    			.convertRatesTo(TimeUnit.SECONDS)
    			.convertDurationsTo(TimeUnit.MILLISECONDS)
    			.build(new File(csvPath)))
    			.start(reporterInterval, TimeUnit.SECONDS);
    }
}