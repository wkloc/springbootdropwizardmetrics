package com.pgs.controller;

import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.health.HealthCheck.Result;
import com.pgs.healthchecks.DataBaseHealthCheck;
import com.pgs.healthchecks.UserCenterHealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;

@RestController
public class HealthCheckController 
{
    private final HealthCheckRegistry registry;    
 
    @Autowired
    public HealthCheckController(HealthCheckRegistry registry) {
        this.registry = registry;

		this.registry.register("userCenter", new UserCenterHealthCheck("unsupported"));
		this.registry.register("databaseCenter", new DataBaseHealthCheck("postgres"));
    }
     
    @RequestMapping(value="/healthStatus", method=RequestMethod.GET)    
    public Set<Entry<String, Result>> getStatus(){
        return registry.runHealthChecks().entrySet();
    }
}