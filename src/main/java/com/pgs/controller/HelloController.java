package com.pgs.controller;

import org.springframework.web.bind.annotation.RestController;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import com.pgs.dto.Credentials;
import com.pgs.service.api.GreetingService;
import com.pgs.service.api.LoginService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@RestController
public class HelloController {    
		
	private final MetricRegistry metrics;
	private final Meter meter;
	
	private final Counter successfullLogins;
	private final Histogram credentialSizes;
	private final Timer responseTimer;

	
	@Autowired
	public HelloController(MetricRegistry metrics) {
		this.metrics = metrics;
		
		this.meter = metrics.meter("greetings.requests");
		this.successfullLogins = metrics.counter("successfulLogins");
		this.credentialSizes = metrics.histogram("credentialSizes");
		this.responseTimer = metrics.timer("responseTime");
	}		
	
	@Autowired 
	LoginService loginService;	
	@Autowired
	GreetingService greetingService;
	
    @RequestMapping("/")
    @Metered  // <-- this finally works
    public String welcome() {
    	return "Greetings from Spring Boot!";
    }
        
	@Timed(absolute = true, name = "loginRunTime") // <-- this finally works
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody String login(@RequestBody Credentials credentials) {
		boolean loginSuccess = loginService.login(credentials.getUsername(), credentials.getPassword());
		if (loginSuccess) {
			credentialSizes.update(credentials.getUsername().length() + credentials.getPassword().length());
			successfullLogins.inc();
		} else {
			successfullLogins.dec();
		}
		return String.valueOf(loginSuccess);
	}
        
    @RequestMapping(value="/logut", method=RequestMethod.POST)    
    public @ResponseBody String logout(@RequestBody String username) {
        return String.valueOf(loginService.logout(username));
    }
    
    @ExceptionMetered(absolute = true) // <-- this finally works
    @RequestMapping(value="/greeting", method=RequestMethod.GET)    
    public @ResponseBody String greeting(@RequestParam("id") int number) throws Exception {
      	return greetingService.getGreeting(number);        
    }
}
