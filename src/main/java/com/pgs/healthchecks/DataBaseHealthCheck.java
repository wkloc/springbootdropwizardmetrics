package com.pgs.healthchecks;

import com.codahale.metrics.health.HealthCheck;

public class DataBaseHealthCheck extends HealthCheck {

	private final String dataBaseName;
	
	public DataBaseHealthCheck(String dataBaseName) {
		this.dataBaseName = dataBaseName;
	}
	
	@Override
	protected Result check() throws Exception {
		if (dataBaseName.isEmpty() || dataBaseName.contains("postgres")) {
			return HealthCheck.Result.unhealthy("Unsupported database: " + dataBaseName);			
        } else {
        	return HealthCheck.Result.healthy();
        }
	}

}
