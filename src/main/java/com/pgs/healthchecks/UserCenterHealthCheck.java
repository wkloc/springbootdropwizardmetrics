package com.pgs.healthchecks;

import com.codahale.metrics.health.HealthCheck;

public class UserCenterHealthCheck extends HealthCheck {
	
	private final String userCenterName;
	
	public UserCenterHealthCheck(String userCenterName) {
		this.userCenterName = userCenterName;
	}

	@Override
	protected Result check() throws Exception {
		if (userCenterName.contains("unsupported")) {
			return HealthCheck.Result.unhealthy("UserCenter is unsuported");			
		} else {
			return HealthCheck.Result.healthy();
		}
	}
}
