package com.pgs.service.api;

public interface GreetingService {
	String getGreeting(int number) throws Exception;
}
