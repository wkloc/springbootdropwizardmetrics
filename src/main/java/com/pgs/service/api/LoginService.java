package com.pgs.service.api;

public interface LoginService {
	boolean login(String userName, String password);
	boolean logout(String userName);
}
