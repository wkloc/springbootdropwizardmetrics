package com.pgs.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.pgs.service.api.GreetingService;


@Service
public class GreetingServiceImpl implements GreetingService{
	
	@Override
	public String getGreeting(int number) throws Exception {		
		if (number < 1 || number > 4) {
			throw new Exception(String.format("No greeting for number #%d", number));
		}
		return "Hello, nice to see tyou there!";
	}
}
