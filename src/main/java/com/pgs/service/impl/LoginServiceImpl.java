package com.pgs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.pgs.service.api.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	     
    @Override
    public boolean login(String userName, String password) {        
        if (userName.equals(password))
        	return false;
        return true;
    }

	@Override
	public boolean logout(String userName) {
		return true;
	}
}
