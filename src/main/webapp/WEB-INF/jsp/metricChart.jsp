<html>
  <head>
    <!--Load the AJAX API-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
     
      function drawChart() {
        $.get("http://localhost:7070/metricGraphData",function(metricData) {
    		var data = google.visualization.arrayToDataTable(metricData, false);
        	// Set chart options        	
        	var options = {'title' : 'Website Metric',
                       	   	'hAxis' : {title : 'Time',titleTextStyle : {color : '#333'}},
                       	   	'vAxis' : {minValue : 0},
                       		'width':1000};        	
        	var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        	chart.draw(data, options);
        });
      }
    </script>
  </head>

  <body>    
    <div id="chart_div" style="width: 1000px; height: 500px;"></div>
  </body>
</html>